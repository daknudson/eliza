#### Eliza - Your online therapist!
## Requirements

[Node.js](https://nodejs.org/) v17.7+

[Go](https://go.dev/doc/install) v1.18.3

To start the client, run the following

```sh
cd frontend

yarn run dev
```

To start the server, run the following

```sh
cd backend

go run main.go
```

Good job! You have successfully booted this application. Try typing something!