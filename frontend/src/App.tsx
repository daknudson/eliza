import { useState } from "react";
import { AiOutlineArrowUp } from 'react-icons/ai'
import { ElizaConnectAdapter } from "./adapters/elizaConnectAdapter";
import { ElizaMediator } from "./mediators/elizaMediator";

function App() {
  const [inputValue, setInputValue] = useState("");
  const [messages, setMessage] = useState<
    { fromMe: boolean; message: string }[]
  >([]);
  const adapter = new ElizaConnectAdapter();
  const mediator = new ElizaMediator(adapter);

  return (
<>
      <header className='header'>Talk to Eliza!</header>
      <div className="app-background">
        <div className="message-button-container">
          <div className="app-message-card">
        <ul className="messages-container">
          {messages.map((msg, index) => (
            <li key={index} className={msg.fromMe ? "eliza-messages" : "userMessages"}>
              <p className={msg.fromMe ? "eliza-message" : "userMessage"}>
                {`${msg.message}`}
              </p>
            </li>
          ))}
        </ul>
        <form
          onSubmit={async (e) => {
            e.preventDefault();
            setInputValue("");
            setMessage((prev) => [
              ...prev,
              {
                fromMe: true,
                message: inputValue,
              },
            ]);
            const response = await mediator.talkToEliza(inputValue);
            setMessage((prev) => [
              ...prev,
              {
                fromMe: false,
                message: response.sentence,
              },
            ]);
          }}
        >
              <div className='user-input-container'>
                <input type="text" value={inputValue} onChange={e => setInputValue(e.target.value)} placeholder='Send Eliza A Message!' className='text-input' />
                <button type='submit' className='send-button'><AiOutlineArrowUp size={25} color='cyan' /></button>
              </div>
              </form>
          </div>
        </div>
      </div>
</>
  )
}

export default App;
